<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery1.8.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/flowplayer-3.2.6.min.js"></script>

<a id="audio" style="display:block;width:648px;height:30px;" href="/show-media-portlet/audio/track06.mp3"></a>

<script type="text/javascript">
    jQuery(document).ready(function(){
    flowplayer("audio","/show-media-portlet/js/flowplayer-3.2.15.swf",
		{
	    plugins: {
	        controls: {
	            fullscreen: false,
	            autoHide: false
	        },
	        audio: {
	            url: '/show-media-portlet/js/flowplayer.audio-3.2.10.swf'
	        }
	    },	
		clip:  {
				autoPlay: true,
				autoBuffering: true,
				provider:"audio"
		    }
		}
	);
  });
</script>

